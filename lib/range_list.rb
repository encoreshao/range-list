# frozen_string_literal: true

require_relative './array'

# RangeList allow to add/remove range
class RangeList
  def initialize
    @data = []
  end

  def add(range)
    return @data if invalid?(range)

    add_or_update(range)
  end

  def remove(range)
    return @data if invalid?(range)

    remove_or_update(range)
  end

  def print
    @data.collect { |item| item.to_s.gsub(']', ')') }.join(' ')
  end

  private

  def invalid?(range)
    !range.is_a?(Array) || range.length != 2
  end

  # Two cases of add actions
  #
  #   1/ new: can be add it into data directly.
  #   2/ overlap: 1) if fully exist, just skip it. 2) if partially covered, need to replace exist item.
  def add_or_update(range)
    return @data if replace_existing_item(range)

    @data << range
  end

  def replace_existing_item(range)
    range_first, range_last = range
    updated = false

    # Detect the new range is overlapping in old item.
    @data.each_with_index do |item, idx|
      next if (item.entries & range.entries).empty?

      new_item    = item
      new_item[0] = range_first if range_first < item[0]
      new_item[1] = range_last if range_last > item[-1]

      @data[idx] = new_item
      updated = true

      break
    end

    updated
  end

  #  Complex removal function
  #
  #  1/ if the range exist, just remove it.
  #  2/ if range and item overlap, need to modify the boundary value.
  #  3/ if the range is within the item, need to split the item according to the range.
  def remove_or_update(range)
    range_first, range_last = range
    range_items             = range.entries
    deleted_indexes         = []

    @data.each_with_index do |item, idx|
      item_first, item_last = item
      overlap_items = item.entries & range_items

      # If the item and range completely overlap, just delete and jump out
      # If the range contains item, need to delete, and to continue
      if (item.entries - range_items).empty?
        deleted_indexes << idx
        break if item.entries == range_items

      # check for differences in overlaps and ranges
      elsif overlap_items.length == range_items.length
        # replace first item
        if range_first == item_first && range_last < item_last
          @data[idx] = [range_last, item_last]

        # split one arr to two arrs
        elsif range_first > item_first && range_last < item_last
          @data[idx] = [item_first, range_first]
          @data.insert(idx + 1, [range_last, item_last])
        end

      # when overlap items less than the new range entries
      elsif overlap_items.length < range_items.length
        if range_first > item_first && range_first < item_last
          @data[idx] = [item_first, range_first]
        elsif range_first < item_first && item_first < range_last && range_last < item_last
          @data[idx] = [range_last, item_last]
        end
      end
    end

    # Important:
    #
    #   After inserting a new item, the data index will change, therefore,
    #     overlapping items in the range should be removed at the end
    deleted_indexes.each { |idx| @data.delete_at(idx) }

    @data
  end
end
