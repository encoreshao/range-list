# frozen_string_literal: true

# Implements entries to eaily build array object by a pair of integers
class Array
  def entries
    (self[0]..self[-1]).to_a
  end
end
