# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../lib/range_list'

# Test for RangeList
class TestRangeList < Minitest::Test
  def setup
    @range_list = RangeList.new
  end

  def test_that_add_print_empty
    @range_list.add([1])

    assert_equal '', @range_list.print
  end

  def test_that_add_print_one
    @range_list.add([1, 5])

    assert_equal '[1, 5)', @range_list.print
  end

  def test_that_add_print_two
    @range_list.add([1, 5])
    @range_list.add([10, 20])

    assert_equal '[1, 5) [10, 20)', @range_list.print
  end

  def test_that_add_print_nochanged_for_duplicate
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])

    assert_equal '[1, 5) [10, 20)', @range_list.print
  end

  def test_that_add_print_replace_latest
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])

    assert_equal '[1, 5) [10, 21)', @range_list.print
  end

  def test_that_add_print_exist_skipping
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])
    @range_list.add([2, 4])

    assert_equal '[1, 5) [10, 21)', @range_list.print
  end

  def test_that_add_print_update_exist
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])
    @range_list.add([2, 4])
    @range_list.add([3, 8])

    assert_equal '[1, 8) [10, 21)', @range_list.print
  end

  def test_that_remove_print_nochanges
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])
    @range_list.add([2, 4])
    @range_list.add([3, 8])

    @range_list.remove([10])

    assert_equal '[1, 8) [10, 21)', @range_list.print
  end

  def test_that_remove_print_replace_item
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])
    @range_list.add([2, 4])
    @range_list.add([3, 8])

    @range_list.remove([10, 11])

    assert_equal '[1, 8) [11, 21)', @range_list.print
  end

  def test_that_remove_print_split_arrs
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])
    @range_list.add([2, 4])
    @range_list.add([3, 8])

    @range_list.remove([10, 11])
    @range_list.remove([15, 17])

    assert_equal '[1, 8) [11, 15) [17, 21)', @range_list.print
  end

  def test_that_remove_print_replace_and_delete
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])
    @range_list.add([2, 4])
    @range_list.add([3, 8])

    @range_list.remove([10, 11])
    @range_list.remove([15, 17])
    @range_list.remove([3, 19])

    assert_equal '[1, 3) [19, 21)', @range_list.print
  end

  def test_that_remove_print_just_delete
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])
    @range_list.add([2, 4])
    @range_list.add([3, 8])

    @range_list.remove([10, 11])
    @range_list.remove([15, 17])
    @range_list.remove([3, 19])
    @range_list.remove([1, 3])

    assert_equal '[19, 21)', @range_list.print
  end

  def test_that_remove_print_add_and_split
    @range_list.add([1, 5])
    @range_list.add([10, 20])
    @range_list.add([20, 20])
    @range_list.add([20, 21])
    @range_list.add([2, 4])
    @range_list.add([3, 8])

    @range_list.remove([10, 11])
    @range_list.remove([15, 17])
    @range_list.remove([3, 19])
    @range_list.remove([1, 3])

    @range_list.add([1, 21])
    @range_list.remove([11, 20])

    assert_equal '[1, 11) [20, 21)', @range_list.print
  end
end
