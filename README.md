# RangeList

## Usage

### Installation

```ruby
bundle install
```

### Implement the RangeList class

- add `minitest` gem to help implement test cases
- add `rubocop` gem to autocorrect code formatting
- use an instance variable `data` to stored data
- add method `invalid?` to verify that the input range is valid
- implements `Array#entries` to easily build an array object from a pair of integers
- use `insert` to add new item to the data object
- Use `delete_at` to delete overlapping items in a range

### Test

```ruby
❯ ruby -Ilib:test test/range_list_test.rb
Run options: --seed 54552

# Running:

..........

Finished in 0.001657s, 6035.0030 runs/s, 6035.0030 assertions/s.
10 runs, 10 assertions, 0 failures, 0 errors, 0 skips
```

### Rubocop

```ruby
❯ bundle exec rubocop -a
```